# NLP Topic Modeling

4th year specific project in Natural Language Processing at INSA Lyon.

The goal is to determine the events in french newpapers.

## Data

- `newspapers_sample.jsonl` : 100 articles
- `newspapers.jsonl` : 100 000 articles

Structure :
```json
{
    "text":"Valence | Un jour une œuvre Un jour, une œuvre: encre de Chine de Wols\n\nLe Dauphiné Libéré - Aujourd'hui à 19:12 - Temps de lecture :",
    "title":"Valence | Un jour une œuvre. Un jour, une œuvre: encre de Chine de Wols",
    "date":"2023-08-10T00:00:00.000",
    "article":"https:\/\/www.ledauphine.com\/culture-loisirs\/2023\/08\/10\/un-jour-une-oeuvre-encre-de-chine-de-wols"
}
```

Columns :
- `title` : title of the newspaper, will be used for the topic
- `text` : content in the newspaper, also be used to determine the topic
- `date` : date of publication, format like : `2022-03-07T00:00:00.000`
- `article` : link the see the article
- `source` : publisher of the article

Structure filtred
```json
{
    "title": "Vosges. Fraize : un piéton heurté par un véhicule",
    "text": "Vosges Fraize : un piéton heurté par un véhicule\n\nPar S. de G. - Aujourd'hui à 18:00 | mis à jour aujourd'hui à 18:03 - Temps de lecture :",
    "date": "2022-12-01",
    "article_id": null,
    "article_url": "https://www.vosgesmatin.fr/faits-divers-justice/2022/12/01/fraize-un-pieton-heurte-par-un-vehicule",
    "article_domain": "vosgesmatin.fr"
}
```

## Dependancies

- `jupyter` : install jupyter notebook
- `pip install -r requirements.txt` : install librairies
- `python -m spacy download fr_core_news_sm` : download the french spacy model
- `python -m spacy download fr_core_news_md` : other spacy model (for word vector)




