
# Python

gensim problem :
- Gensim use triu from scipy witch is deprecated
- To use triu you need scipy 1.10 or lower
- I can't downgrade triu lower than 1.11 because I'm using a too recent version of python (3.13)

# NLP

1. **Text Preprocessing**:
- **Tokenization**: Splitting the text into individual words or tokens.
- **Lowercasing**: Converting all text to lowercase to ensure consistency.
- **Removing Punctuation**: Eliminating punctuation marks from the text.
- **Removing Stopwords**: Removing common words like "the," "is," "and," etc., which do not carry significant meaning for topic modeling.
- **Stemming or Lemmatization**: Reducing words to their root form to consolidate similar words (e.g., "running" and "ran" to "run").

2. **Feature Extraction**:
- **Bag of Words (BoW)**: Representing text data as a matrix where rows correspond to documents and columns correspond to unique words. The matrix contains word frequencies or binary indicators.
- **Term Frequency-Inverse Document Frequency (TF-IDF)**: Assigning weights to words based on their frequency in the document and inverse frequency across documents. This helps prioritize rare words that might be more informative.

3. **Topic Modeling Algorithm**:
- **Latent Dirichlet Allocation (LDA)**: A probabilistic generative model that represents each document as a mixture of topics and each topic as a mixture of words. LDA uncovers latent topics present in the corpus.
- **Non-Negative Matrix Factorization (NMF)**: Decomposes the document-term matrix into two lower-dimensional matrices, one representing document-topic relationships and the other representing topic-term relationships. It's especially useful when interpretability is a priority.

4. **Model Evaluation**:
- **Perplexity**: Measures how well the model predicts the held-out data. Lower perplexity indicates better performance.
- **Coherence Score**: Measures the interpretability of topics by calculating the semantic similarity between high-scoring words within topics.
- **Visualization**: Using techniques like t-SNE or UMAP to visualize high-dimensional topic distributions in 2D or 3D space.

5. **Post-Processing and Interpretation**:
- **Topic Labeling**: Assigning human-readable labels to topics based on the most representative words.
- **Topic Analysis**: Analyzing the content of documents assigned to each topic to understand the underlying themes.

# Notes and ressources

- [spacy](https://spacy.io/usage/spacy-101) : how to install and use spacy, explain the tokenisation, POS tagging, similarity, ...
- hugging face
- bert topic


import matplotlib.pyplot as plt # visualize data
import numpy as np # for arrays and math functions
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=42)

- [introduction to spacy](http://spacy.pythonhumanities.com/intro.html)
- [associated youtube video](https://www.youtube.com/watch?v=dIUTsFT2MeQ)


Document Term Matrix - DTM
:   number of times each words appear in each lines of the data

Latent Dirichlet allocation - LSA
:   





how to choose top words :
- select n elements
- selects first(s) elements (multiple if ex aequo)
- select with elbow method (until big change in the derivate)




ideas :
- remove stop words
- DTM
- vectorise text/title and use simple clustering techniques (k-means, dbscan, random forest, ...)
- LSA
- LDA
- transformers


Make a 2D graph (words/date) and adapt the word order to see if it makes a line/branches


Linear Discriminent Analysis - LDA
:	Project into a bigger space and then recuce the dimension by projetting on the "orthogonale" of the hyperplan that separate the most the data

Let $\mu _1$, $s_1^2$ and $\mu _2$, $s_2^2$ the average and standard deviation of the two classes.

We want $\frac{(\mu_1-\mu_2)^2}{s_1^2+s_2^2}$ to as great as possible








tf/idf

nmf




comment représenter 5 mots à l'aide d'un seul





text -> corpus
text -> tidy text






trouver le topic != trouver les mots clés



Liste des [mots clés qui correspondent à un texte] != Liste des [textes qui correspondent aux mots clés]

l1[keys] = texte != l2[texte] = keys

Pareil pour le topic






